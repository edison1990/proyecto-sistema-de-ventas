<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// aqui se agregaran todas las rutas posibles que tendra el sistema. recordar que las rutas son procesadas por un //controlador, los cuales, en caso de tener que traer alguna informcion, interactuan con el modelo(base de datos) //y llaman a una vista para mostrar la informacion 

// el tipo de ruta "resource" crea un grupo de rutas con peticiones como index, create, destroy, show, store, edit, update. la cual esta conectada a un controldor.

// cuando se ingresa a la vista negocio/categoria, el controlador(categoriaControler) y en este se agregaran las funciones de create, edit, etc...

//-----> recordar buscar informacion sobre como agregar controladores con artisan en el simbolo del sistema <-----

Route::resource('negocio/categoria','categoriaController');
Route::resource('negocio/producto','productoController');
Route::resource('ventas/clientes','clienteController');
Route::resource('compras/proveedores','ProveedorController');
Route::resource('compras/ingresos','IngresoController');