<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    protected $table="Ingreso"; // esto dice a la clase "model Categoria" que el nombre de la tabla que ira a buscar a la base de datos sera la que tiene asignada la variable $table.
    protected $primaryKey="IdIngreso"; //al igual que $table="Categoria" este le indica que la primary key sera el valor asignado a la variable $primaryKey.

    public $timestamps=false; //esto evita que se creen dos columnas que por defecto deberian estar en la base de datos "created_at" y "updated_at".


    // este establece que en mi modelo solo se puede insertar o actualizar los campos que estan a continuacion 
    protected $fillable= [   
    'IdProveedor',
    'TipoDocumento',
    'NumeroDocumento',
    'FechaHora',
    'Impuesto',
    'Estado'
    ];
}
