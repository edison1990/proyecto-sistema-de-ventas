<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\RequestFormIngreso;
use App\Ingreso;
use App\DetalleIngreso;
use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;


class IngresoController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request) //recibe parametro un objeto de tipo Request llamado request(tipo: Request, objeto: $request)
    {
    	if($request)// si existe el objeto request entonces se obtienen todos los registros de la tabla categoria.
    	{
            $query=trim($request->get('searchText'));
            $ingresos=DB::table('Ingreso')
            ->join('Persona','Ingreso.IdProveedor','=','Persona.IdPersona')
            ->join('DetalleIngreso','Ingreso.IdIngreso','=','DetalleIngreso.IdIngreso')
            // funcion DB::raw =  es una expresion sin procesar en una consulta, es mas rapida ya que se inyecta directamente en la consulta
            //como una cadena de texto por lo que el generador de consultas de laravel no verifica ni valida nada 
            // ejemplo: $result = DB::select("SELECT * FROM users"); $result2 = DB::select(DB::raw("SELECT * FROM users")); 
            ->select('Ingreso.IdIngreso','Ingreso.FechaHora','Persona.Nombre','Ingreso.TipoDocumento','Ingreso.NumeroDocumento','Ingreso.Impuesto','Ingreso.Estado',DB::raw('sum(DetalleIngreso.Cantidad*PrecioCompra) as total '))
            ->where('Ingreso.NumeroDocumento','LIKE','%'.$query.'%')
            ->orderBy('Ingreso.IdIngreso','desc')
            ->groupBy('Ingreso.IdIngreso','Ingreso.FechaHora','Persona.Nombre','Ingreso.TipoDocumento','Ingreso.NumeroDocumento','Ingreso.Impuesto','Ingreso.Estado')
            ->paginate(10);
            return view('compras.ingresos.index',["ingresos"=>$ingresos,"buscarTexto"=>$query]);

    	}
}

public function create()
{
  $personas=DB::table('Persona')->Where('TipoPer','=','Proveedor')->get();
  $productos=DB::table('Producto')
  ->select(DB::raw('CONCAT(Producto.Codigo, " " , Producto.Nombre) as producto'),'Producto.IdProducto')
  ->where('Producto.Estado','=','Activo')
  ->get();
  return view("compras.ingresos.create",["personas"=>$personas,"productos"=>$productos]);
}


public function store(RequestFormIngreso $request)
{
    try{//capturdor de excepciones
        DB::beginTransaction();// este metodo de transacciones nos permite insertar o consultar datos a la base de datos
        //en mas de una tabla, asegurandoe que durante el proceso no ocurra ningun error y asi no insertr datos incompletos en la BD, para 
        // esto esta el metodo rollback que regresa todo a su estado anterior si al falla. 
        $ingreso=new Ingreso;
        $ingreso->IdProveedor=$request->get('idproveedor');
        $ingreso->TipoDocumento=$request->get('tipodocumento');
        $ingreso->NumeroDocumento=$request->get('numdocumento');
        $capturafechahora = Carbon::now('Chile/Continental');
        $ingreso->FechaHora=$capturafechahora->toDateTimeString();
        $ingreso->Impuesto='19';
        $ingreso->Estado='Activo';
        $ingreso->save();

        $idproducto=$request->get('idproducto');
        $cantidad=$request->get('cantidad');
        $preciocom=$request->get('preciocom');
        $precioven=$request->get('precioven');

        $cont=0;

        while($cont < count($idproducto)){
            $detalle = new DetalleIngreso();
            $detalle->IdIngreso= $ingreso->IdIngreso;
            $detalle->IdProducto= $idproducto[$cont];
            $detalle->Cantidad= $cantidad[$cont];
            $detalle->PrecioCompra= $preciocom[$cont];
            $detalle->PrecioVenta= $precioven[$cont];
            $detalle->save();
            $cont=$cont+1;

        }

        DB::commit();  //commit (guardar en base de datos) solo si no hubieron errores en el try de lo contrario 
                      //pasaria al catch con el metodo  DB::rollback(); osea vuelta al estado inicial, sin guardar nada
    }catch(exception $e)
    {
        DB::rollback(); //rollback (vuelta al estado inicial, sin guardar nada)
    }

    return redirect::to('compras/ingresos');
}

public function show($id)
{
    $ingreso=Db::table('Ingreso')
    ->join('Persona','Ingreso.IdProveedor','=','Persona.IdPersona')
    ->join('DetalleIngreso','Ingreso.IdIngreso','=','DetalleIngreso.IdIngreso')
    ->select('Ingreso.IdIngreso','Ingreso.FechaHora','Persona.Nombre','Ingreso.TipoDocumento','Ingreso.NumeroDocumento','Ingreso.Impuesto','Ingreso.Estado',DB::raw('sum(DetalleIngreso.Cantidad*PrecioCompra) as total '))
    ->where('Ingreso.IdIngreso','=',$id)
    ->groupBy('Ingreso.IdIngreso', 'Ingreso.FechaHora', 'Persona.Nombre', 'Ingreso.TipoDocumento', 'Ingreso.NumeroDocumento','Ingreso.Impuesto', 'Ingreso.Estado')
    ->first();

    $detalles=DB::table('DetalleIngreso')
    ->join('Producto','DetalleIngreso.IdProducto','=','Producto.IdProducto')
    ->select('Producto.Nombre as producto','DetalleIngreso.Cantidad','DetalleIngreso.PrecioCompra','DetalleIngreso.PrecioVenta')
    ->where('DetalleIngreso.IdIngreso','=',$id)
    ->get();
    return view("compras.ingresos.show",["ingreso"=>$ingreso,"detalles"=>$detalles]);

}

public function destroy($id)
{
    $ingreso=Ingreso::findOrFail($id);
    $ingreso->Estado='Cancelado';
    $ingreso->update();
    return redirect::to('compras/ingresos');
}

}