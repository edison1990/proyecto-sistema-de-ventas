<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\recuestFormproducto;
use DB;


class productoController extends Controller
{
     
   public function __construct()
   {

   }

   public function index(Request $request) //recibe parametro un objeto de tipo Request llamado request(tipo: Request, objeto: $request)
   {
       if($request)// si existe el objeto request entonces se obtienen todos los registros de la tabla categoria.
       {
           $query=trim($request->get('buscarTexto'));
           $productos= DB::table('Producto')
           ->join('Categoria','Producto.IdCategoria','=','Categoria.IdCategoria')
           ->select('Producto.IdProducto','Producto.Nombre','Producto.Codigo','Producto.Stock','Categoria.Nombre as nombrecat','Producto.Descripcion','Producto.Imagen','Producto.Estado')
           ->where('Producto.Nombre','LIKE','%'.$query.'%')
           ->orwhere('Producto.Codigo','LIKE','%'.$query.'%')
           ->orderBy('IdProducto','desc')
           ->paginate(10);
           
           return view('negocio.producto.index',["productos"=>$productos,"buscarTexto"=>$query]);

       }

   }

   public function create()
   {
       $categorias=DB::table('Categoria')->where('Estado','=','1')->get();
       return view('negocio.producto.create',['categorias'=>$categorias]);
       /*al hacer click en el boton 'nuevo' solo me redirecciona a la vista 'create'. pero al haber un formulario de tipo'POST' en esta vista, tambien me llama a la funcion 'STORE()' de mi controlador, donde se hara el envio del formulario junto a sus validaaciones para ser insertados a travez del MODELO en la base de datos.
       validaciones => Request/RecuestFormcategoria. */
   }

   public function store(recuestFormproducto $request)//validacion almacenada en variable request
   {
       $producto=new Producto; //en esta variable categoria se almacena un nuevo objeto del MODELO categoria.
       $producto->IdCategoria=$request->get('idcategoria'); //captura nombre de formulario, valida los datos con var Req 
       $producto->Codigo=$request->get('codigo');
       $producto->Nombre=$request->get('nombre');
       $producto->Stock=$request->get('stock');
       $producto->Descripcion=$request->get('descripcion');// lo mismo que el nombre pero ahora con descripcion.
       $producto->Estado='Activo';// el dato Estado se ingresa por defecto en '1', para que al redireccionar se valide en el formulario index y muestre el nuevo registro en la tabla
       if ($request->hasFile('imagen')){
           $file=$request->file('imagen');
           $file->move(public_path().'/imagenes/productos/', $file->getClientOriginalName());
           $producto->Imagen=$file->getClientOriginalName();
         }
       $producto->save();// guarda los datos ingresados en la base de datos.

       return Redirect::to('negocio/producto');//redirecciona a la vista principal.

   }

   public function edit($id)
   {
       $producto=Producto::findOrFail($id);
       $categorias = DB::table('Categoria')->where('Estado','=','1')->get();
       return view('negocio.producto.editar',['producto'=>$producto,'categorias'=> $categorias]);
   }

   public function show($id)
   {
       return view('negocio.producto.show',['producto'=>$producto::findOrFail($id)]);
   }

   public function update(requestFormcategoria $request, $id)
   {
       $categoria= Categoria::findOrFail($id);
       $producto=new Producto; //en esta variable categoria se almacena un nuevo objeto del MODELO categoria.
       $producto->IdCategoria=$request->get('idcategoria'); //captura nombre de formulario, valida los datos con var Req 
       $producto->Codigo=$request->get('codigo');
       $producto->Nombre=$request->get('nombre');
       $producto->Stock=$request->get('stock');
       $producto->Descripcion=$request->get('descripcion');// lo mismo que el nombre pero ahora con descripcion.
      
       if ($request->hasFile('imagen')){
        $file=$request->file('imagen');
        $file->move(public_path().'/imagenes/productos/', $file->getClientOriginalName());
        $producto->Imagen=$file->getClientOriginalName();
      }

       $producto->save();// guarda los datos ingresados en la base de datos.
     
       $producto->update();
       return redirect('negocio/producto');
       

   }

   public function destroy($id)
   {
       $producto= Producto::findOrFail($id);
       $producto->Estado='Inactivo';
       $producto->update();
       return redirect::to('negocio/producto');

   } 

}
