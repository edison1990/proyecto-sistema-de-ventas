<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\RequestFormPersona;
use DB;

class ClienteController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request) //recibe parametro un objeto de tipo Request llamado request(tipo: Request, objeto: $request)
    {
    	if($request)// si existe el objeto request entonces se obtienen todos los registros de la tabla categoria.
    	{
    		$query=trim($request->get('buscarTexto'));
            $personas = DB::table('Persona') 
            ->where('Nombre','LIKE','%'.$query.'%')
            ->where('TipoPer','=','Cliente')
            ->orwhere('Rut','LIKE','%'.$query.'%')
            ->where('TipoPer','=','Cliente')
    		
    		->orderBy('IdPersona','desc')
    		->paginate(10);
            
            return view('ventas.clientes.index',["personas"=>$personas,"buscarTexto"=>$query]);

    	}

    }

    public function create()
    {
    	return view('ventas.clientes.create');
    	/*al hacer click en el boton 'nuevo' solo me redirecciona a la vista 'create'. pero al haber un formulario de tipo'POST' en esta vista, tambien me llama a la funcion 'STORE()' de mi controlador, donde se hara el envio del formulario junto a sus validaaciones para ser insertados a travez del MODELO en la base de datos.
    	validaciones => Request/RecuestFormcategoria. */
    }

    public function store(RequestFormPersona $request)//validacion almacenada en variable request
    {
        $persona=new Persona; //en esta variable categoria se almacena un nuevo objeto del MODELO categoria.
        $persona->TipoPer='Cliente';
    	$persona->Nombre=$request->get('nombre'); //captura nombre de formulario, valida los datos con var Req 
        $persona->Rut=$request->get('rut');// lo mismo que el nombre pero ahora con descripcion.
        $persona->Direccion=$request->get('direccion');
        $persona->Telefono=$request->get('telefono');
        $persona->Email=$request->get('mail');
    	
    	$persona->save();// guarda los datos ingresados en la base de datos.

    	return Redirect::to('ventas/clientes');//redirecciona a la vista principal.

    }

    public function edit($id)
    {
        return view('ventas.clientes.editar',['persona'=>Persona::findOrFail($id)]);
    }

    public function show($id)
    {
        return view('ventas.clientes.show',['persona'=>Persona::findOrFail($id)]);
    }

    public function update(RequestFormPersona $request, $id)
    {
        $persona= Persona::findOrFail($id);
        //$persona->TipoPer='Cliente';
    	$persona->Nombre=$request->get('nombre'); 
        $persona->Rut=$request->get('rut');
        $persona->Direccion=$request->get('direccion');
        $persona->Telefono=$request->get('telefono');
        $persona->Email=$request->get('mail');

    	$persona->update();
    	return redirect('ventas/clientes');
    	

    }

    public function destroy($id)
    {
    	$persona= Persona::findOrFail($id);
    	$persona->TipoPer='Inactivo';
        $persona->update();
        return redirect::to('ventas/clientes');

    } 
 
}
