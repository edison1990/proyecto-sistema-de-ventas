<?php

namespace App\Http\Controllers;

// se agregan las rutas hacia nustras carpetas Modelo/Categoria, Http\ controller y request para asignarles funciones a estas a traves de este controlador para Categoria 

use Illuminate\Http\Request;
use App\Http\Requests; 
use App\Categoria;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\RecuestFormCategoria;
use DB;


class categoriaController extends Controller
{
    // aqui se agregan las funciones create, update, etc... las que seran utilizadas por el request en la ruta:resource. se comienza con un consructor.

   public function __construct()
    {

    }

    public function index(Request $request) //recibe parametro un objeto de tipo Request llamado request(tipo: Request, objeto: $request)
    {
    	if($request)// si existe el objeto request entonces se obtienen todos los registros de la tabla categoria.
    	{
    		$query=trim($request->get('buscarTexto'));
    		$categorias = DB::table('Categoria') ->where('Nombre','LIKE','%'.$query.'%')
    		->where('Estado','=','1')
    		->orderBy('IdCategoria','desc')
    		->paginate(10);
            
            return view('negocio.categoria.index',["categorias"=>$categorias,"buscarTexto"=>$query]);

    	}

    }

    public function create()
    {
    	return view('negocio.categoria.create');
    	/*al hacer click en el boton 'nuevo' solo me redirecciona a la vista 'create'. pero al haber un formulario de tipo'POST' en esta vista, tambien me llama a la funcion 'STORE()' de mi controlador, donde se hara el envio del formulario junto a sus validaaciones para ser insertados a travez del MODELO en la base de datos.
    	validaciones => Request/RecuestFormcategoria. */
    }

    public function store(RecuestFormCategoria $request)//validacion almacenada en variable request
    {
    	$categoria=new Categoria; //en esta variable categoria se almacena un nuevo objeto del MODELO categoria.
    	$categoria->Nombre=$request->get('nombre'); //captura nombre de formulario, valida los datos con var Req 
    	$categoria->Descripcion=$request->get('descripcion');// lo mismo que el nombre pero ahora con descripcion.
    	$categoria->Estado='1';// el dato Estado se ingresa por defecto en '1', para que al redireccionar se valide en el formulario index y muestre el nuevo registro en la tabla
        
        $categoria->save();// guarda los datos ingresados en la base de datos.

    	return Redirect::to('negocio/categoria');//redirecciona a la vista principal.

    }

    public function edit($id)
    {
        return view('negocio.categoria.editar',['categoria'=>Categoria::findOrFail($id)]);
    }

    public function show($id)
    {
        return view('negocio.categoria.show',['categoria'=>Categoria::findOrFail($id)]);
    }

    public function update(RecuestFormCategoria $request, $id)
    {
        $categoria= Categoria::findOrFail($id);
    	$categoria->Nombre=$request->get('nombre');
    	$categoria->Descripcion=$request->get('descripcion');
    	$categoria->update();
    	return redirect('negocio/categoria');
    	

    }

    public function destroy($id)
    {
    	$categoria= Categoria::findOrFail($id);
    	$categoria->Estado='0';
        $categoria->update();
        return redirect::to('negocio/categoria');

    } 


}
