<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestFormIngreso extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
    'IdProveedor'=>'max:30',
    'TipoDocumento'=>'max:30',
    'NumeroDocumento'=>'max:30',
    'IdProducto' =>'max:30',
    'Cantidad'=>'max:30',
    'PrecioCompra'=>'max:30',
    'PrecioVenta'=>'max:30',
    
        ];
    }
}
