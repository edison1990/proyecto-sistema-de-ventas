<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecuestFormCategoria extends FormRequest
{
    /**
     * Determine si el usuario está autorizado para hacer esta solicitud.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // se cambia por true para autorizar los request
    }

    /**
     * Obtenga las reglas de validación que se aplican a la solicitud.
     *
     * @return array
     */

    // con esta funcion se establecen las validaciones que hara un formulario al ser completado ej: campos obligatorios, cantidad de letras, etc... 
    public function rules()
    {
        return [ // aqui se ponen los nombres de los campos del formulario que se quiere validar.
            'nombre' => 'required|max:50',
            'nombre' => 'required|max:50',
            'descripcion' => 'max:300',
        ];
    }
}
