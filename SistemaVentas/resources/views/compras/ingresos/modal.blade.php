<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" 
     id="modal-delete-{{$in->IdIngreso}}">


{{Form::Open(array('action'=>array('IngresoController@destroy',$in->IdIngreso), 'method'=>'delete'))}}

<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title">Cancelar Ingreso</h5>
      </div>
      <div class="modal-body">
        <p>Desea cancelar este Ingreso?  <h2>{{$in->Nombre}} </h2></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Confirmar</button>
        
      </div>
    </div>
  </div>

{{Form::close()}}
