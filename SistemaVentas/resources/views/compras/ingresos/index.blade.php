@extends ('layouts.admin')
@section('contenido')

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3> Ingresos <a href="ingresos/create"><button class="btn btn-success">Nuevo</button> </a> </h3>
		 @include('compras.ingresos.search')

		<!-- {!! Form::open(array('url'=>'negocio/categoria','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}
<div class="form-group">
	<div class="input-group">
	<input type="text" class="form-control" name="buscartexto" placeholder="buscar..." value="{{$buscarTexto}}">
	<span class="input-group-btn">
		<button type="submit" class="btn btn.primary">Buscar</button>
     </span>
	</div>
</div>

{{Form::close()}}-->

	</div>	


</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Id</th>
					<th>Fecha</th>
					<th>Proveedor</th>
					<th>Tipo de documento</th>
					<th>Numero de documento</th>
					<th>IVA</th>
					<th>Total</th>
					<th>Estado</th>
					<th>Opciones</th>
					
				</thead>
                @foreach ($ingresos as $in)
				<tr>
					<td>{{$in->IdIngreso}}</td>
					<td>{{$in->FechaHora}}>
					<td>{{$in->Nombre}}</td>
					<td>{{$in->TipoDocumento}}</td>
					<td>{{$in->NumeroDocumento}}</td>
					<td>{{$in->Impuesto}}</td>
					<td>{{$in->total}}</td>
					<td>{{$in->Estado}}</td>

				
					<td>
					
						<a href="{{URL::action('IngresoController@show', $in->IdIngreso)}}"><button class="btn btn-primary">Ver detalles</button></a>
						<a href="" data-target="#modal-delete-{{$in->IdIngreso}}" data-toggle="modal"><button class="btn btn-danger">Cancelar ingreso</button></a>
					</td>
				</tr>
				@include('compras.ingresos.modal')
				@endforeach
			</table>
		</div>
		{{$ingresos->render()}}
	</div>
</div>

@endsection