@extends ('layouts.admin')
@section('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3> Nuevo ingreso</h3>

		@if(count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)

				<li>{{$error}}</li>

				@endforeach


			</ul>
		</div>
		@endif
	</div>	
</div>
        {!! Form::open(array('url'=>'compras/ingresos','method'=>'POST','autocomplete'=>'off'))!!}
        {{Form::token()}}<!--campo oculto que protege contra ataques CSRF= Cross-site request forgery o falsificación de petición en sitios cruzados.
        es un método por el cual un usuario malintencionado intenta hacer que los usuarios, sin saberlo, envíen datos que no quieren enviar. 
         -->
		<div class="row">
	    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
         	<label for="proveedor">Proveedor</label>
         	<select name="idproveedor" id="idperoveedor" class="form-control selectpicker"  data-live-search="true">
			 @foreach($personas as $persona)
			 <option value="{{$persona->IdPersona}}">{{$persona->Nombre}}</option>
			 @endforeach
			 </select>
         </div>
			</div>

			
	    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label>Documento</label>
			 <select name="tipodocumento" class="form-control">
			 <option value="Factura">Factura</option>
			 <option value="Boleta">Boleta</option>
			 <option value="Guia">Guia</option>
			 </select>
         	
         </div>
			</div>

			

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="numdocumento">Numero documento</label>
         	<input type="text" class="form-control" name="numdocumento" required value="{{old('numdocumento')}}" placeholder="numero documeno..">
			</div>
			</div>
			</div>

			
			<div class="row">
			<div class="panel panel-primary">
			<div class="panel-body">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<div class="form-group">
			<label>Producto</label>
			<select name="pproducto" class="form-control selectpicker" id="pproducto" data-live-search="true">
			@foreach($productos as $producto)
			 <option value="{{$producto->IdProducto}}">{{$producto->producto}}</option>
			 @endforeach
			 </select>
            </div>
			</div> 
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
			<div class="form-group">
			<label for="pcantidad">Cantidad</label>
			<input type="number" name="pcantidad" id="pcantidad" class="form-control" placeholder="cantidad">
			</div>
			</div>

			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
			<div class="form-group">
			<label for="pcompra">Precio Compra</label>
			<input type="number" name="pcompra" id="pcompra" class="form-control" placeholder="Precio compra">
			</div>
			</div>

			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
			<div class="form-group">
			<label for="pventa">Precio Venta</label>
			<input type="number" name="pventa" id="pventa" class="form-control" placeholder="Precio venta">
			</div>
			</div>

			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
			<div class="form-group">
			<button type="button" id="btnagregar" name="btnagregar" class="btn btn-primary">Agregar</button>
			</div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<table id="detalle" class="table table-striped table-bordered table-condensed table-hover">
			<thead>
			<th>Opciones </th>
			<th>Articulo</th>
			<th>Cantidad</th>
			<th>Precio Compra </th>
			<th>Precio Venta </th>
			<th>Subtotal</th>
			</thead>
			<tfoot>
			<th>Total </th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th><h4 id="total">$/ 0.00 <h/4></th>
			</tfoot>
			</table>
			</div>
	
		</div>
			</div>
			</div>
			

              
            
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="guardar">
	    	<div class="form-goup">
			<input name="_token" value="{{csrf_token()}}" type="hidden"></input>
        	<button class="btn btn-success" type="submit">Guardar</button>
        	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>
			</div>
			</div>
			
			<!--</div>-->

        {{Form::close()}}

		@push('scripts')
		<script>
		$(document).ready(function(){
			$('#btnagregar').click(function(){
				agregar();
			});
		});

		var cont=0;
		total=0;
		subtotal=[];
		$("#guardar").hide();

		function agregar()
		{
			idproducto=$("#pproducto").val();
			producto=$("#pproducto option:selected").text();
			cantidad=$("#pcantidad").val();
			preciocom=$("#pcompra").val();
			precioven=$("#pventa").val();

			if (idproducto!="" && cantidad!="" && cantidad>0 && preciocom!="" && precioven!="")
			{
				subtotal[cont]=(cantidad*preciocom);
				total=total+subtotal[cont];

				var fila='<tr class="selected" id="fila'+cont+'"><td> <button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button> </td><td> <input type="hidden" name="idproducto[]" value="'+idproducto+'">'+producto+'</td><td> <input type="number" name="cantidad[]" value="'+cantidad+'"></td><td> <input type="number" name="preciocom[]" value="'+preciocom+'"></td><td> <input type="number" name="precioven[]" value="'+precioven+'"></td><td>'+subtotal[cont]+'</td></tr>';
				cont++;
				limpiar();
				$("#total").html("$/ "+total);
				evaluar();
				$('#detalle').append(fila);

			}
			else{

				alert("error al ingresar, verifique los datos de los campos requeridos");
			}
			
		}

		

		function limpiar(){
		$("#pcantidad").val("");
		$("#pcompra").val("");
		$("#pventa").val("");
		}

		function eliminar(index){
			total=total-subtotal[index]
			$("#total").html("$/ "+total);
			$("#fila" + index).remove();
		}

		function evaluar()
  {
    if (total>0)
    {
      $("#guardar").show();
    }
    else
    {
      $("#guardar").hide(); 
    }
   }



		</script>

		@endpush

@endsection
