@extends ('layouts.admin')
@section('contenido')

		<div class="row">
	    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
         	<label for="proveedor">Proveedor</label>
         <p> {{$ingreso->Nombre}} </p>
         </div>
			</div>

			
	    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label>Documento</label>
			 <p> {{$ingreso->TipoDocumento}} </p>
         </div>
			</div>

			

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="numdocumento">Numero documento</label>
         	<p> {{$ingreso->NumeroDocumento}} </p>
			</div>
			</div>
			

			
			<div class="row">
			<div class="panel panel-primary">
			<div class="panel-body">
          
			

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<table id="detalle" class="table table-striped table-bordered table-condensed table-hover">
			<thead>
			
			<th>Producto</th>
			<th>Cantidad</th>
			<th>Precio Compra </th>
			<th>Precio Venta </th>
			<th>Subtotal</th>
			</thead>
			<tfoot>
			
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th><h4 id="total">{{$ingreso->total}} <h/4></th>
			</tfoot>
		    <tbody>
			@foreach($detalles as $det)
			<tr>
			<td> {{$det->producto}} </td>
			<td> {{$det->Cantidad}} </td>
			<td> {{$det->PrecioCompra}} </td>
			<td> {{$det->PrecioVenta}} </td>
			<td> {{$det->Cantidad*$det->PrecioCompra}} </td>
			</tr>

			@endforeach
			
			</tbody>
			</table>
			</div>
	
		</div>
			</div>
			</div>
			

@endsection
