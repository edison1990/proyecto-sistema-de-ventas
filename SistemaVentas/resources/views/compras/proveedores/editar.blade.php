@extends ('layouts.admin')
@section('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3> Editar proveedores: {{$persona->Nombre}} </h3>

		@if(count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)

				<li>{{$error}}</li>

				@endforeach


			</ul>
		</div>
		@endif

	</div>
</div>
         <!-- en esta parte del formulario me dio error en la ruta, donde es negocio.categoria.update, el error decia
         que la ruta no estaba definida, lo solucione quitando la primera parte de la ruta "negocio", porque? nose -->
		{!! Form::model($persona,['method'=>'PATCH','route'=>['proveedores.update', $persona->IdPersona]])!!}
       
        {{Form::token()}}<!--campo oculto que protege contra ataques CSRF= Cross-site request forgery o falsificación de petición en sitios cruzados.
        es un método por el cual un usuario malintencionado intenta hacer que los usuarios, sin saberlo, envíen datos que no quieren enviar. 
         -->

		 <div class="row">
	    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="nombre">Nombre</label>
         	<input type="text" class="form-control" name="nombre" required value="{{$persona->Nombre}}" >
         </div>
			</div>

			

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="rut">Rut</label>
         	<input type="text" class="form-control" name="rut" required value="{{$persona->Rut}}">
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="direccion">Direccion</label>
         	<input type="text" class="form-control" name="direccion" required value="{{$persona->Direccion}}" >
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="telefono">Telefono</label>
         	<input type="text" class="form-control" name="telefono" value="{{$persona->Telefono}}">
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="mail">E-mail</label>
         	<input type="text" class="form-control" name="mail" value="{{$persona->Email}}">
			</div>
			</div>
			
           
		  
            
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	    	<div class="form-goup">
        	<button class="btn btn-success" type="submit">Guardar</button>
        	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>
			</div>
			
		



        {{Form::close()}}

@endsection