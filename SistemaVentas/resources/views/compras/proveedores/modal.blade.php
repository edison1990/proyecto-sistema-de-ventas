<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" 
     id="modal-delete-{{$per->IdPersona}}">


{{Form::Open(array('action'=>array('ProveedorController@destroy',$per->IdPersona), 'method'=>'delete'))}}

<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title">Eliminar proveedor</h5>
      </div>
      <div class="modal-body">
        <p>Desea eliminar este proveedor?  <h2>{{$per->Nombre}} </h2></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Confirmar</button>
        
      </div>
    </div>
  </div>

{{Form::close()}}
