@extends ('layouts.admin')
@section('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3> Registrar proveedor</h3>

		@if(count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)

				<li>{{$error}}</li>

				@endforeach


			</ul>
		</div>
		@endif
	</div>	
</div>
        {!! Form::open(array('url'=>'compras/proveedores','method'=>'POST','autocomplete'=>'off'))!!}
        {{Form::token()}}<!--campo oculto que protege contra ataques CSRF= Cross-site request forgery o falsificación de petición en sitios cruzados.
        es un método por el cual un usuario malintencionado intenta hacer que los usuarios, sin saberlo, envíen datos que no quieren enviar. 
         -->
		<div class="row">
	    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="nombre">Nombre</label>
         	<input type="text" class="form-control" name="nombre" required value="{{old('nombre')}}" placeholder="nombre del cliente...">
         </div>
			</div>

			
	    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="rut">Rut</label>
         	<input type="text" class="form-control" name="rut" required value="{{old('rut')}}" placeholder="Rut del cliente...">
         </div>
			</div>

			

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="direccion">Direccion</label>
         	<input type="text" class="form-control" name="direccion" required value="{{old('direccion')}}" placeholder="Direccion cliente...">
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="telefono">Telefono</label>
         	<input type="text" class="form-control" name="telefono" required value="{{old('telefono')}}" placeholder="fono del cliente...">
			</div>
			</div>


			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="mail">E-mail</label>
         	<input type="text" class="form-control" name="mail" value="{{old('mail')}}" placeholder="email cliente...">
			</div>
			</div>
		
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	    	<div class="form-goup">
        	<button class="btn btn-success" type="submit">Guardar</button>
        	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>
			</div>
			




        {{Form::close()}}

@endsection
