@extends ('layouts.admin')
@section('contenido')

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3> Clientes <a href="clientes/create"><button class="btn btn-success">Nuevo</button> </a> </h3>
		 @include('ventas.clientes.search')

		<!-- {!! Form::open(array('url'=>'negocio/categoria','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}
<div class="form-group">
	<div class="input-group">
	<input type="text" class="form-control" name="buscartexto" placeholder="buscar..." value="{{$buscarTexto}}">
	<span class="input-group-btn">
		<button type="submit" class="btn btn.primary">Buscar</button>
     </span>
	</div>
</div>

{{Form::close()}}-->

	</div>	


</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Id</th>
					<th>Nombre</th>
					<th>Rut</th>
					<th>Direccion</th>
					<th>telefono</th>
					<th>E-mail</th>
					<th>Opciones</th>
					
				</thead>
                @foreach ($personas as $per)
				<tr>
					<td>{{$per->IdPersona}}</td>
					<td>{{$per->Nombre}}</td>
					<td>{{$per->Rut}}</td>
					<td>{{$per->Direccion}}</td>
					<td>{{$per->Telefono}}</td>
					<td>{{$per->Email}}</td>
				
					<td>
					
						<a href="{{URL::action('clienteController@edit', $per->IdPersona)}}"><button class="btn btn-warning">Editar</button></a>
						<a href="" data-target="#modal-delete-{{$per->IdPersona}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
					</td>
				</tr>
				@include('ventas.clientes.modal')
				@endforeach
			</table>
		</div>
		{{$personas->render()}}
	</div>
</div>

@endsection