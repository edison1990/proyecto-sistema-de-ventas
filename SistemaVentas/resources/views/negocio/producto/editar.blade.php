@extends ('layouts.admin')
@section('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3> Editar Productos: {{$producto->Nombre}} </h3>

		@if(count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)

				<li>{{$error}}</li>

				@endforeach


			</ul>
		</div>
		@endif

	</div>
</div>
         <!-- en esta parte del formulario me dio error en la ruta, donde es negocio.categoria.update, el error decia
         que la ruta no estaba definida, lo solucione quitando la primera parte de la ruta "negocio", porque? nose -->
		{!! Form::model($producto,['method'=>'POST','route'=>['producto.update', $producto->IdProducto], 'files'=>'true'])!!}
       
        {{Form::token()}}<!--campo oculto que protege contra ataques CSRF= Cross-site request forgery o falsificación de petición en sitios cruzados.
        es un método por el cual un usuario malintencionado intenta hacer que los usuarios, sin saberlo, envíen datos que no quieren enviar. 
         -->

		 <div class="row">
	    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="nombre">Nombre</label>
         	<input type="text" class="form-control" name="nombre" required value="{{$producto->Nombre}}" >
         </div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
			<label>Categoria</label>
			<select name="idcategoria" class="form-control"> 
			@foreach($categorias as $cat)
			@if($cat->IdCategoria==$producto->IdCategoria)
            <option value="{{$cat->IdCategoria}}" selected>{{$cat->Nombre}}</option>
			@endif
			@endforeach
			</select>
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="codigo">Codigo</label>
         	<input type="text" class="form-control" name="codigo" required value="{{$producto->Codigo}}">
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="stock">Stock</label>
         	<input type="text" class="form-control" name="stock" required value="{{$producto->Stock}}" >
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="descripcion">Descripcion</label>
         	<input type="text" class="form-control" name="descripcion" value="{{$producto->Descripcion}}" placeholder="descripcion del producto...">
			</div>
			</div>
			

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="imagen">Imagen</label>
         	<input type="file" class="form-control" name="imagen">
			 @if(($producto->Imagen)!="")
			 <img src="{{asset('imagenes/productos/'.$producto->Imagen)}}" height="200px" width="200px">
			 @endif
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	    	<div class="form-goup">
        	<button class="btn btn-success" type="submit">Guardar</button>
        	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>
			</div>
			




        {{Form::close()}}

@endsection