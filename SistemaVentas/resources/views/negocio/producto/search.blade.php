{!! Form::open(array('url'=>'negocio/producto','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}
<div class="form-group">
	<div class="input-group">
	<input type="text" class="form-control" name="buscarTexto" placeholder="buscar..." value="{{$buscarTexto}}">
	<span class="input-group-btn">
		<button type="submit" class="btn btn.primary">Buscar</button>
     </span>
	</div>
</div>

{{Form::close()}}
