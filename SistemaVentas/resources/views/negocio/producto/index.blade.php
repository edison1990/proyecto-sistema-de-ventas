@extends ('layouts.admin')
@section('contenido')

<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3> Productos <a href="producto/create"><button class="btn btn-success">Nuevo</button> </a> </h3>
		 @include('negocio.producto.search')

		<!-- {!! Form::open(array('url'=>'negocio/categoria','method'=>'GET','autocomplete'=>'off','role'=>'search'))!!}
<div class="form-group">
	<div class="input-group">
	<input type="text" class="form-control" name="buscartexto" placeholder="buscar..." value="{{$buscarTexto}}">
	<span class="input-group-btn">
		<button type="submit" class="btn btn.primary">Buscar</button>
     </span>
	</div>
</div>

{{Form::close()}}-->

	</div>	


</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Id</th>
					<th>Nombre</th>
					<th>Codigo</th>
					<th>Categoria</th>
					<th>Stock</th>
					<th>Imagen</th>
					<th>Estado</th>
					<th>Opciones</th>
				</thead>
                @foreach ($productos as $pro)
				<tr>
					<td>{{$pro->IdProducto}}</td>
					<td>{{$pro->Nombre}}</td>
					<td>{{$pro->Codigo}}</td>
					<td>{{$pro->nombrecat}}</td>
					<td>{{$pro->Stock}}</td>
					<td> <img src="{{asset('imagenes/productos/'.$pro->Imagen)}}" alt="{{$pro->Nombre}}" height="100px" width="100px" class="img-thumbnail"> </td>
					<td>{{$pro->Estado}}</td>
					<td>
						<a href="{{URL::action('productoController@edit', $pro->IdProducto)}}"><button class="btn btn-warning">Editar</button></a>
						<a href="" data-target="#modal-delete-{{$pro->IdProducto}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
					</td>
				</tr>
				@include('negocio.producto.modal')
				@endforeach
			</table>
		</div>
		{{$productos->render()}}
	</div>
</div>

@endsection