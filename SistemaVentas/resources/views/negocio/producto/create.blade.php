@extends ('layouts.admin')
@section('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3> Añadir nuevo producto</h3>

		@if(count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)

				<li>{{$error}}</li>

				@endforeach


			</ul>
		</div>
		@endif
	</div>	
</div>
        {!! Form::open(array('url'=>'negocio/producto','method'=>'POST','autocomplete'=>'off', 'files'=>'true'))!!}
        {{Form::token()}}<!--campo oculto que protege contra ataques CSRF= Cross-site request forgery o falsificación de petición en sitios cruzados.
        es un método por el cual un usuario malintencionado intenta hacer que los usuarios, sin saberlo, envíen datos que no quieren enviar. 
         -->
		<div class="row">
	    	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="nombre">Nombre</label>
         	<input type="text" class="form-control" name="nombre" required value="{{old('nombre')}}" placeholder="nombre producto...">
         </div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
			<label>Categoria</label>
			<select name="idcategoria" class="form-control"> 
			@foreach($categorias as $cat)
            <option value="{{$cat->IdCategoria}}">{{$cat->Nombre}}</option>
			@endforeach
			</select>
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="codigo">Codigo</label>
         	<input type="text" class="form-control" name="codigo" required value="{{old('codigo')}}" placeholder="codigo de producto">
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="stock">Stock</label>
         	<input type="text" class="form-control" name="stock" required value="{{old('stock')}}" placeholder="stock...">
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="descripcion">Descripcion</label>
         	<input type="text" class="form-control" name="descripcion" value="{{old('descripcion')}}" placeholder="descripcion del producto...">
			</div>
			</div>
			

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
         	<label for="imagen">Imagen</label>
         	<input type="file" class="form-control" name="imagen">
			</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	    	<div class="form-goup">
        	<button class="btn btn-success" type="submit">Guardar</button>
        	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>
			</div>
			




        {{Form::close()}}

@endsection
