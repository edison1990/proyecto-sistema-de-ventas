<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" 
     id="modal-delete-{{$cat->IdCategoria}}">


{{Form::Open(array('action'=>array('categoriaController@destroy',$cat->IdCategoria), 'method'=>'delete'))}}

<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title">Eliminar Categoria</h5>
      </div>
      <div class="modal-body">
        <p>Desea eliminar esta categoria?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        
      </div>
    </div>
  </div>

{{Form::close()}}
