@extends ('layouts.admin')
@section('contenido')

<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3> Editar Categoria: {{$categoria->Nombre}} </h3>

		@if(count($errors)>0)
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)

				<li>{{$error}}</li>

				@endforeach


			</ul>
		</div>
		@endif
         <!-- en esta parte del formulario me dio error en la ruta, donde es negocio.categoria.update, el error decia
         que la ruta no estaba definida, lo solucione quitando la primera parte de la ruta "negocio", porque? nose -->
		{!! Form::model($categoria,['method'=>'PATCH','route'=>['categoria.update', $categoria->IdCategoria]])!!}
       
        {{Form::token()}}<!--campo oculto que protege contra ataques CSRF= Cross-site request forgery o falsificación de petición en sitios cruzados.
        es un método por el cual un usuario malintencionado intenta hacer que los usuarios, sin saberlo, envíen datos que no quieren enviar. 
         -->

         <div class="form-group">
         	<label for="nombre">Nombre</label>
         	<input type="text" class="form-control" name="nombre" value="{{$categoria->Nombre}}" placeholder="nombre...">
         </div>

         <div class="form-group">
         	<label for="descripcion">Descripcion</label>
         	<input type="text" class="form-control" name="descripcion" value="{{$categoria->Descripcion}}" placeholder="descripcion...">
         </div>

        <div class="form-goup">
        	<button class="btn btn-success" type="submit">Guardar</button>
        	<button class="btn btn-danger" type="reset">Cancelar</button>
        </div>



        {{Form::close()}}

 

	</div>

</div>

@endsection